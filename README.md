# README #

This README is used for reproducing the Table 2, 3 and 4 for the essay: *The performance of TCP/IP for networks with high bandwidth-delay products and random loss*

### Introduction ###

Today, TCP has become the most important transport layer protocol in the networking area. However, there are still some factors will influence the performance of TCP in real life. In these factors, bandwidth, delay, and random loss are the most important three factors. In the essay The performance of TCP/IP for networks with high bandwidth-delay products and random loss [1], the author uses the analytical and simulated method to show us which factor is more important for TCP performance in different conditions. Because TCP Reno is a window controlled protocol. In both slow start or congestion avoid step, the window size almost impacts everything. bandwidth-delay product has a such big influence on window size. So the essential thought is to conclude the relationship between bandwidth-delay product and link utilization. This project focuses on designing experiments to reproduce the Table 2, 3 and 4 in the IEEE essay above mentioned. Here, link utilization is designed as the metric of the TCP performance. Table 2 shows the relationship between the link utilization and the normalized buffer size when bandwidth and propagation delay is fixed. Table 3 shows how the random loss probability influences the link utilization. Table 4 combines all the factors together as a new one. Using the product of the loss probability and the square of the bandwidth-delay product to discover the change of utilization to show the performance of TCP Reno.

The whole reproduction could be finished in 60-80 minutes.


### Background ###

In order to compare the analysis, testbed and simulation results of the performance of TCP congestion control protocol Reno. We design three separate testbed experiments. Then put the data and the analytical results together to show the differences. Because all these experiments are based on single connection, we don't need to set multiple connections. What’s more, because TCP Tahoe is no longer available in the modern Linux kernel, we only reproduce the Reno part in the three tables.We are going to measure the utilization according to the changes of the different parameters.There are seven important parameters during reproducing the three tables, they are:

* **Bandwidth μ** :  In the essay, the unit of the bandwidth is: packet. So it can’t be set directly in the instaGeni topology. Then we need another related parameter capacity.
* **Capacity C**: Based on bandwidth value. It can be directly set in instaGeni topology. Because the MTU of packets in TCP flows is 1500 Bytes, so the formula to get capacity according to bandwidth should be: C = 1500*8*μ . Here the unit of capacity is bps.
* **Propagation delay τ**: all delays except for service time and queueing at the bottleneck link are lumped into a single “propagation delay,” 
* **Random loss probability q**: When packet transmits in the single link, it may be lost with probability q, and that these random losses are independent. 
* **FIFO buffer size B**: A FIFO buffer which is set in the interface of the router side. Packets arrive after the buffer is full will be lost.
* **Normalized buffer size β**: β is a computed parameter according of B, μ and τ.  All β in the experiment is defined as: β =B/(μτ+1)
* **Utilization ρ**: In all the three stages, link utilization equals to throughput divides capacity.

First, the model that is used is a 3 nodes client-server model:
![Screen Shot 2016-05-15 at 18.18.29.png](https://bitbucket.org/repo/pk5Lo6/images/619364529-Screen%20Shot%202016-05-15%20at%2018.18.29.png)

Totally, we need to generate three experiments for the three tables:
For Table 2 shown in the essay, it shows the relationship of utilization ρ and the product of bandwidth μ and delay τ without random loss.
Put the results the author gave us in figure, it is like: (this is also the one we need to reproduce)
![2.png](https://bitbucket.org/repo/pk5Lo6/images/2286474574-2.png)

The factor we will vary in this the experiment for Table 2 is is normalized buffer size β, and for each β, we have a pair of buffer size B,

**1. β = 0.1,      B = 10**

**2. β = 0.2,      B = 20**

**3. β = 0.31,     B = 31**

**4. β = 0.32,     B = 32**

**5. β = 0.8,      B = 80**

Fix μ = 100, C = 1200Mbps, τ = 1ms. Each pair should be run 5 times to get the average utilization.

**Table 3** fix the μ and τ, and change the random loos probability q as five values with two different normalized buffer size  β.

![3.png](https://bitbucket.org/repo/pk5Lo6/images/312406952-3.png)

The factor we will vary in this the experiment for Table 3 is normalized buffer size β and random loss probability q. Because Propagation delay τ and Bandwidth μ is separately fixed to 100 and 1, we have two values for is **0.8** and **0.2**. And for each we assign 7 random loss probability (**0.1, 0.01, 0.001, 0.0001, 0.00001, 0.000001, 0**) to them. So there will be totally 14 pairs of factors. 


**Table 4** shows the strong dependence of link utilization on the product of random loss probability and the product of bandwidth-delay q(μτ)^2. It shows the link utilization for several different bandwidth- delay products when the normalized buffer size β is fixed at 0.8. 


![4.png](https://bitbucket.org/repo/pk5Lo6/images/2519616301-4.png)

The factor we will vary in this the experiment for Table 4 is q(μτ)^2 , the product of the loss probability q and BDP. It is set to 0, 1, 10. And each value, we separate it into three conditions:

* For q(μτ)^2 = 10:

C=1200Mbps, τ=1ms, q=0.001 B=81 

C=2400Mbps, τ=1ms, q=0.00025 B=161

C = 2400Mbps, τ = 2ms, q = 0.0000625  B = 321

* For q(μτ)^2 = 1:

C=1200Mbps, τ=1ms, q=0.0001 B=81

C = 2400Mbps, τ = 1ms, q = 0.000025  B = 161

C = 2400Mbps, τ = 2ms, q = 0.00000625  B = 321

* For q(μτ)^2 = 0:

C=1200Mbps, τ=1ms, q=0 B=81 

C=2400Mbps, τ=1ms, q=0 B=161 

C=2400Mbps, τ=2ms, q=0 B=321

So for each condition we need 3 random loss probability to make sure the three bandwidth-delay product values. Then we will get the utilization.


### Results ###

There is only three tables mentioned in the essay, so I plot the tables as the figure.

For **Tables 2**, the results should be:
![1.png](https://bitbucket.org/repo/pk5Lo6/images/773654192-1.png)

We run 5 times for each normalized buffer size, and then compute the average value, the data will be more convinced. After 25 pairs of data , my reproduce figure is:
![4.png](https://bitbucket.org/repo/pk5Lo6/images/3433481144-4.png)

In the table the author gave us, these two lines should be very closed. And the difference of utilization is small for large β. But in my testbed result, it seems to have some errors. In the point where normalized buffer size is 0.8, the results are smaller than the analytical one. It is normal that testbed result has some errors rather than the simulation, especially when in the ideal condition(when β is close to one). But the result tendency is correct. The utilization increases when the normalized buffer size increases. Although it does not increase linearly, but it is quite stable. Because the SS step in Reno won’t be repeated a lot of times, and each congestion avoid step is identical, when the buffer size becomes larger and larger, the utilization will be closer to 1.

Table 2 is not contained the core thought in this essay because it even does not consider the product of the bandwidth and delay or loss probability. I think it is just a table that the author wants to show the analytical process and let the reader get ready to understand the following tables.

For **Tables 3**, the result two figures should be:
![2.png](https://bitbucket.org/repo/pk5Lo6/images/3865127342-2.png)

This time, I should fix two normalized buffer size as 0.8 and 0.2. For each β, we assign 7 random loss probability to it, from 10% to 0%, we want to see the tendency when other parameter fixed only but loss probability change, then I do the reproduction for the Table 3:

* For β =0.8, the relationship of utilization and the random loss probability is:
![5.1.png](https://bitbucket.org/repo/pk5Lo6/images/2707115435-5.1.png)

* For β =0.2, the relationship of utilization and the random loss probability is:

![5.2.png](https://bitbucket.org/repo/pk5Lo6/images/1515484924-5.2.png)

We consider two figures above together and get a conclusion from the plot. For the two values of normalized buffer size with a fixed value of bandwidth-delay product. My testbed result can’t suit the analytical result well as the simulation one in the essay, but the qualitative observations are identical. The utilization increases rapidly first and when going up to touch 1, it slows down.
From both two figures, when random loss probability is greater than 0.001, the utilization is very low, less than 0.4. That link utilization will waste a lot of bandwidth resources. That means when there is a significant loss ( probability larger than 0.001), the TCP preforms very bad. And when it is less than 0.0001, the performance is very good.
Also, compared the two figures, when random loss probability is greater than 0.001, the loss probability dominated the utilization instead of the buffer size. But when it is less than 0.0001, different normalized buffer value gives us different utilization. When buffer size is large, the utilization can be very closed to the ideal condition. However, when the buffer is not enough for coming packets, even if there is almost no lost during transition, the utilization is only between 0.8 and 0.9.

This reproduction of Table 3 tells us how the random loss probability influences on the utilization alone. It is such an important parameter in TCP Reno congestion control, that is why we need to avoid packets loss all the time in our lives.

At last, it is time to show how the product of random loss probability q and the square of BDP (μτ)^2 influence the utilization.

For **Table 4**, we totally consider 3  q(μτ)^2. It is set to 0, 1, 10. And for each value, we separate it into three conditions with different bandwidth and propagation delay, the result is: 

![6.png](https://bitbucket.org/repo/pk5Lo6/images/125786952-6.png)

We can notice when q(μτ)^2 is 10, no mater how bandwidth and delay change, it won’t increase too much. But when q(μτ)^2 becomes to 1, the value of utilization bursts to 0.9. It tells us the most important factor in TCP Reno connection is not the bandwidth nor the propagation delay or loss probability. It is the product of all of them q(μτ)^2. Random loss can cause a significant utilization deterioration while the product of the loss probability and the square of BDP is greater than one.



### Run My Experiment ###

* Use the command below to check your Kernel Version:
       
         cat /proc/version

My kernel version is 3.19.0-28-generic

* Use the command below to check your linux Version:
       
         lsb_release -a

My Linux version is Ubuntu 14.04.3 LTS 

* The GENI aggregate I used is  GENI Rack that provide compute and network resources available




**So, Let's start!!!!!!!**

At first, because our experiment need to be done under the TCP Reno environment. So we should make sure the congestion control algorithm is Reno.

**How to Set the TCP congestion control algorithm as Reno.**

Use the command below to see what is the default algorithm for TCP flows now.
           
           sysctl net.ipv4.tcp_available_congestion_control
If the result is net.ipv4.tcp_congestion_control = reno, its good. We don't need to change it any more. If not use the command below to get a list of congestion control algorithms that are available in your kernel (if you are running 2.6.20 or higher):
           
           sysctl net.ipv4.tcp_available_congestion_control
In most Linux version, the Reno algorithm is available. Then use the command below to change the default algorithm to Reno:
          
           sysctl -w net.ipv4.tcp_congestion_control=reno
Then the setting of Reno is finished, you can repeat the step 1 to determine again.

To start, just create a normal new slice on the GENI portal, using any name. Create a client-router-server as the  topology used to be. And using the same guidelines for choosing an aggregate.There is no other special parameters to set in GENI for this experiment.

Once your resources come up, install iperf3 tools and netem tools in your VM. (step not listed here).

Then we should reserve resource in instaGeni, set a 3 node client-server topology and change the two links' capacity as 120000kbps:

Open three VM terminals and log in client, router, server seperately.

##### For Table 2 #####

**How to set the iperf3 sender and receiver:**
Try with the following command on the receiver (server) node:
        
        iperf3 -s -D -p 5005 
Leave that running, and on the sender (client) nodes, run:

        iperf3 -c client 10.10.2.2 -t 60000 -p 5005


And on the router side, we should set a FIFO buffer with the propagation delay we need to set. Here the value is 1 all the time.
Try setting the FIFO queue on interface 1 by running the following command on the router:

        sudo tc qdisc add dev eth1 root pfifo limit 10 

If the system shows any lines like: the qdisc has existed, use the command below to replace it:
  
        sudo tc qdisc replace dev eth1 root pfifo limit 10   
Because we should control the propagation delay, so we add netem parameters on interface 2:
        
        sudo tc qdisc del dev eth2 root
        sudo tc qdisc add dev eth2 root: netem delay 1ms

The tc tool includes a command to show the current state of a queue. Try running it, specifying as the last interface the name of the network interface you want to monitor

        tc -p -s -d qdisc show dev eth2

**You should following the following steps to get the result**:


1. Use tc tool to set the FIFO buffer size with specific buffer size limit and propagation delay τ first.

2. Start iperf3 server on the server node.

3. Start iperf3 TCP flows with the appropriate arguments and with 60 seconds.

4. Wait for the sender to finish, then you can see the throughput in the server side.

5. Use the throughput and the capacity set before to calculate the realized value of utilization.

6. Then change buffer limit to 10, 20, 31 32 81 each time. You totally should run 5 times to get the results.

##### For Table 3 #####

In this part we need to add the parameter random loss probability *q*, and the normalized buffer size is also have two values: 0.8 and 0.2.

Because at last part, we have set most parameters, so u can still use it by adding one command.

The command to set queue parameters should replace the last command to set the loss probability:

       sudo tc qdisc replace dev eth2 root: netem delay 1ms loss 10%

**You should following the following steps to get the result**:

1. Use tc tool to set the FIFO buffer size with specific buffer size limit and propagation delay τ and random loos probability *q* first.

2. Start iperf3 server on the server node.

3. Start iperf3 TCP flow with the appropriate arguments and with 60 seconds.

4. Wait for the sender to finish, then you can see the throughput in the server side.

5. Use the throughput and the capacity set before to calculate the realized value of utilization.

6. For each buffer size B, change *q* to 0.1, 0.01, 0.001, 0.0001, 0.00001, 0.000001, 0 seven values. 

7. And we totally have 2 λ related to the β. When β is 0.8, buffer limit should be set to 81. When β is 0.2, buffer limit should be set to 20. So,we should send the packets 14 times to get the average results.

##### For Table 4 #####

Set another instaGeni topology. This time, set the link capacity to 2400000kbps.

Still use the command above mentioned.

But because the normalized buffer size is fixed to 0.8, so 
* we should keep buffer limit B as 81  when μ=100, τ=1.
* we should keep buffer limit B as 161 when μ=200, τ=1.
* we should keep buffer limit B as 321 when μ=200, τ=2.

And when the bandwidth change from 100 to 200, we should log in the new instageni resources.

For different bandwidth and delay pair,change the buffer limit via command:
       
        sudo tc qdisc replace dev eth1 root pfifo limit **xxx**
 
And keep changing the delay and loos probability according to different requirements.

For different delay and loss probability, keep change buffer size B.

**You should following the following steps to get the result**:

1. Use tc tool to set the FIFO buffer size with specific buffer size limit and propagation delay τ and random loos probability *q* first.

2. Start iperf3 server on the server node.

3. Start iperf3 TCP flow with the appropriate arguments and with 60 seconds.

4. Wait for the sender to finish, then you can see the throughput in the server side.

5. Use the throughput and the capacity set before to calculate the realized value of utilization.

6. We have 3 pairs of the product of loss probability and bandwidth-delay product  q(μτ)^2. And for each of it. We have three pair of different q, μ, τ, λ and B. So we totally have 9 pairs of these 3 parameters.

**For q(μτ)^2 = 10:**

* C = 1200Mbps, τ = 1ms,   q = 0.001      B = 81

* C = 2400Mbps, τ = 1ms,   q = 0.00025    B = 161

* C = 2400Mbps, τ = 2ms,   q = 0.0000625  B = 321

**For q(μτ)^2 = 1:**

* C = 1200Mbps, τ = 1ms, q = 0.0001     B = 81   
 
* C = 2400Mbps, τ = 1ms, q = 0.000025    B = 161

* C = 2400Mbps, τ = 2ms, q = 0.00000625  B = 321

**For q(μτ)^2 = 0:**

* C = 1200Mbps,  τ = 1ms,   q = 0       B = 81   
 
* C = 2400Mbps,  τ = 1ms,   q = 0       B = 161

* C = 2400Mbps,  τ = 2ms,   q = 0       B = 321

**After finishing all the work for Table 2, 3 and 4, use the data you got to reproduce the table and figure we need.**